use std::path::{Path, PathBuf};

use flatc_rust;

fn main() {
    let protos = vec!["message"]
        .into_iter()
        .map(|p| format!("flatbuffers/{}.fbs", p))
        .collect::<Vec<String>>();
    let inputs = protos.iter().map(Path::new).collect::<Vec<_>>();
    flatc_rust::run(flatc_rust::Args {
        lang: "rust", // `rust` is the default, but let's be explicit
        inputs: &inputs,
        includes: &[Path::new("flatbuffers/common.fbs")],
        out_dir: Path::new("./src/"),
        ..Default::default()
    })
    .expect("flatbuffer compilation");
}
