use rand::{self, rngs::OsRng, RngCore};
use sha3::{Digest, Sha3_256};

use srp::groups::G_2048;

#[allow(dead_code, unused_imports)]
#[path = "./message_generated.rs"]
mod message_generated;

pub mod data {
}

pub mod auth {
    use super::*;
    use srp::client::{srp_private_key, SrpClient};
    pub fn start_client<'a>() -> (SrpClient<'a, Sha3_256>, Vec<u8>) {
        let mut a = [0u8; 64];
        OsRng.fill_bytes(&mut a);
        let client = SrpClient::<Sha3_256>::new(&a, &G_2048);
        let a_pub = client.get_a_pub();
        (client, a_pub)
    }

    pub fn validate(salt: &Vec<u8>, srv_pub: &Vec<u8>) -> Vec<u8> {
        vec![]
    }
}

pub mod server {
    use srp::server::{SrpServer, UserRecord};
}
